import { Request, Response } from "express";
import bcrypt from "bcrypt";
import { User } from "../entities/User";


export const getUsers = async (req: Request, res: Response) => {
  try {
    const users = await User.find({
      relations: {
        bookings: true
      }
    });
    return res.json(users);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const getUser = async (req: Request, res: Response) => {
  try {
    const { userId } = req.params;
    const user = await User.findOne({
      where: { id: userId },
      relations: ["bookings"]
    });

    if (!user) return res.status(404).json({ message: "User not found" });

    return res.json(user);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const createUser = async (req: Request, res: Response) => {
  const { email, userName, password } = req.body
  try {
    if (!email || !password) {
      return res.status(400).json({ msg: "Please, verify your email or password" });
    };

    const user = await User.findOneBy({ email: email });
    if (user) {
      return res.status(400).json({ msg: "The user already Exists" });
    };
    const newUser = new User();
    newUser.email = email;
    newUser.userName = userName;
    newUser.password = await bcrypt.hash(password, 10);
    await newUser.save();
    return res.status(201).json(newUser);

  } catch (error) {
    res.status(500).json({ msg: error });
  };
};

export const updateUser = async (req: Request, res: Response) => {
  const { userId } = req.params;
  const { userName, active } = req.body
  try {
    const user = await User.findOneBy({ id: userId });
    if (!user) return res.status(404).json({ message: "User not found" });

    user.userName = userName;
    user.active = active
    await User.save(user)

    return res.status(200).json({ msg: "User updated" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const deleteUser = async (req: Request, res: Response) => {
  const { userId } = req.params;
  try {
    const result = await User.delete({ id: userId });

    if (result.affected === 0)
      return res.status(404).json({ message: "User not found" });

    return res.sendStatus(204).json({ msg: "User deleted" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};