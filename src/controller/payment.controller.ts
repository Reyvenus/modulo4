import { Request, Response } from "express"
import { Booking } from "../entities/Booking"
import { Payment } from "../entities/Payment"
import { Statuses, paymentTypes } from "../constans/constans"


export const createPayment = async (req: Request, res: Response) => {
  const { bookingId, paymentType } = req.body;
  console.log("BODYYY", bookingId, paymentType)

  try {
    const bookingValidate = await Booking.findOneBy({ id: bookingId });
    if (!bookingValidate) return res.status(200).json({ msg: "The booking no exist" });
    if (bookingValidate.status === Statuses.PROCESSED) return res.status(200).json({ msg: "The booking was processed" })
    if (!paymentTypes.includes(paymentType.toLowerCase())) return res.status(200).json({ msg: "The payment type is incorrect" });
    console.log("bookinvalidate", bookingValidate)

    const newPayment = new Payment();
    newPayment.paymentMethod = paymentType;
    newPayment.totalAmount = bookingValidate.total_price;
    newPayment.bookings = [bookingValidate];
    await newPayment.save();

    bookingValidate.status = Statuses.PROCESSED;
    await Booking.save(bookingValidate);

    return res.status(200).json(newPayment);
  } catch (error) {
    return res.status(500).json({ msg: error });
  };
};

export const getPayment = async (req: Request, res: Response) => {
  const { paymentId } = req.params;
  try {

    const payments = await Payment.find();
    if (paymentId) {
      const payment = payments.find(pay => pay.id === paymentId);
      return res.status(200).json(payment);
    }
    return res.status(200).json(payments);
  } catch (error) {
    res.status(500).json({ msg: error });
  };
};
