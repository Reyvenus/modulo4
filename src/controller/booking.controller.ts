import { Request, Response } from "express";
import { User } from "../entities/User";
import { Event } from "../entities/Event";
import { Booking } from "../entities/Booking";


export const createBooking = async (req: Request, res: Response) => {
  const { userId } = req.params;
  const { events } = req.body;
  let arrayEvents: Event[] = [];
  let getEvent;

  try {
    const userValidate = await User.findOneBy({ id: userId });
    if (!userValidate) return res.send("The user not exist");

    for (const event of events) {
      getEvent = await Event.findOneBy({ id: event.eventId });
      if (!getEvent) return res.send(`The Event with id:${event.eventId} not exist`);
      if (getEvent.top_limit <= getEvent.lower_limit) return res.send(`The event "${getEvent.name}" exceeded quota limit`);
      arrayEvents.push(getEvent);
    };

    const newBooking = new Booking();
    newBooking.total_price = arrayEvents.reduce((acc, event) => acc + event.price, 0);
    newBooking.user = userValidate;
    newBooking.events = arrayEvents;
    await newBooking.save();

    for (const event of arrayEvents) {
      await Event.update({ id: event.id }, {
        top_limit: event.top_limit - 1,
        lower_limit: event.lower_limit + 1
      });
    }
    return res.status(200).json(newBooking);

  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const getBooking = async (req: Request, res: Response) => {
  const { bookingId } = req.params;

  try {
    const bookings = await Booking.find({
      relations: ["events"]
    })

    if (bookings.length > 0 && bookingId) {
      const getBooking = bookings.find(booking => booking.id === bookingId);
      if (!getBooking) return res.status(200).json({ msg: "The booking no exist" });
      return res.status(200).json(getBooking);
    }
    res.status(200).json(bookings);

  } catch (error) {
    res.status(500).json({ msg: error });
  }
};

export const updateBooking = async (req: Request, res: Response) => {
  const { bookingId } = req.params
  const { userId, events} = req.body
  let arrayEvents: Event[] = []

  try {
    const booking = await Booking.findOne({
      where: { id: bookingId },
      relations: ["events"]
    });
    const previousEvents: any = booking ? booking.events : undefined
    if (!booking) return res.status(404).json({ message: "The booking no exist" });

    for (const event of events) {
      const getEvent = await Event.findOneBy({ id: event.eventId });
      if (!getEvent) return res.send(`The Event with id:${event.eventId} not exist`)
      if (getEvent.top_limit <= getEvent.lower_limit) return res.send(`The event "${getEvent.name}" exceeded quota limit`)
      arrayEvents.push(getEvent)
    }

    booking.user = userId,
    booking.total_price =  0
    await Booking.save(booking)

    booking.events = arrayEvents
    await Booking.save(booking)

    for (const event of arrayEvents) {
      await Event.update({ id: event.id }, {
        top_limit: event.top_limit - 1,
        lower_limit: event.lower_limit + 1
      });
    }

    for (const event of previousEvents) {
      if (event.top_limit >= (event.lower_limit + event.top_limit)) return;
      await Event.update({ id: event.id }, {
        top_limit: event.top_limit + 1,
        lower_limit: event.lower_limit - 1
      });
    }

    return res.status(200).json({ msg: "The booking was updated" });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

export const deleteBooking = async (req: Request, res: Response) => {
  const { bookingId } = req.params;

  try {
    const bookingValidate = await Booking.findOne({
      where: { id: bookingId },
      relations: ["events"]
    });
    console.log(bookingValidate)
    if (!bookingValidate) return res.status(404).json({ message: "The booking no exist" });

    await Booking.remove(bookingValidate);

    res.status(200).json({ mensaje: "Booking deleted" });

    for (const event of bookingValidate.events) {
      if (event.top_limit >= (event.lower_limit + event.top_limit)) return;
      await Event.update({ id: event.id }, {
        top_limit: event.top_limit + 1,
        lower_limit: event.lower_limit - 1
      });
    }
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    };
  };
};
