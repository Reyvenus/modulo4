import { Request, Response } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { User } from "../entities/User";
import { createToken, jwtRefreshTokenSecret, jwtSecret, refreshTokens } from "../service/createToken";


export const signIn = async (req: Request, res: Response) => {
  const { email, password } = req.body
  try {
    if (!email || !password) {
      return res.status(400).json({ msg: "Please, verify your email or password" });
    }

    const user = await User.findOneBy({ email: email });
    if (!user) {
      return res.status(400).json({ msg: "The User does not exists" });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ msg: "The email or password are incorrect" });
    }
    return res.status(400).json({ credentials: createToken(user) });

  } catch (error) {
    console.log("Errror", error)
    res.status(500).json({ msg: error });
  };
};

export const refresh = async (req: Request, res: Response) => {
  const { refreshToken } = req.body;
  try {
    if (!refreshToken) {
      res.status(401).json({ msg: "Token not found" });
    }

    if (!refreshTokens.includes(refreshToken)) {
      res.status(403).json({ msg: "Invalid refresh token" });
    }

    const user = jwt.verify(refreshToken, jwtRefreshTokenSecret);
    const { email } = <any>user;

    const userFound = await User.findOneBy({ email: email });
    if (!userFound) {
      return res.status(400).json({ msg: "The User does not exists" });
    }
    const accessToken = jwt.sign({ id: userFound.id, email: userFound.email }, jwtSecret, { expiresIn: '120s' });

    return res.json({ accessToken });
  } catch (error) {
    console.log("ERRORRR", error)
    return res.status(403).json({ msg: "Invalid token" });
  }
};
