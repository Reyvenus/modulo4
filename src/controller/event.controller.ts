import { Request, Response } from "express";
import { Event } from "../entities/Event";


export const getEvent = async (req: Request, res: Response) => {
  const { eventId } = req.params

  try {
    const events = await Event.find();
    if (events.length > 0 && eventId) {
      const getEvent = events.find(event => event.id === (eventId));
      if (!getEvent) return res.send("The event not exist");
      return res.status(200).json(getEvent);
    }
    return res.json(events);
  } catch (error) {
    throw error;
  }
};

export const createEvent = async (req: Request, res: Response) => {
  const { name, description, place, gps, price, eventType } = req.body;

  try {
    const newEvent = new Event();
    newEvent.name = name;
    newEvent.description = description;
    newEvent.place = place;
    newEvent.date = new Date(Math.floor(Math.random() * Date.now())).toString();
    newEvent.gps = gps;
    newEvent.price = price;
    newEvent.eventType = eventType;
    await newEvent.save();

    return res.status(200).json(newEvent);

  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const updateEvent = async (req: Request, res: Response) => {
  const { eventId } = req.params;
  const { place, gps, price, eventType } = req.body;

  try {
    const eventValidate = await Event.findOneBy({ id: eventId })
    if (!eventValidate) return res.status(404).json({ message: "The event no exist" });

    await Event.update({ id: eventValidate.id }, {
      place,
      gps,
      price,
      date: new Date(Math.floor(Math.random() * Date.now())).toString(),
      eventType
    });

    return res.status(200).json({ msg: "event updated" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    };
  }
};

export const deleteEvent = async (req: Request, res: Response) => {
  const { eventId } = req.params;

  try {
    const eventValidate = await Event.findOneBy({ id: eventId });
    if (!eventValidate) return res.status(404).json({ message: "The event no exist" });

    await Event.delete({ id: eventValidate.id });

    res.status(200).json({ mensaje: "Event deleted" });

  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    };
  }
};

