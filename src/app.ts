import express from "express"
import morgan from "morgan"
import cors from "cors"
import passport from "passport"
import passportMiddleware from "./middelware/passport";
import userRoute from "./routes/userRoute"
import eventRoute from "./routes/eventRoute"
import bookingRoute from "./routes/bookingRoute"
import paymentRoute from "./routes/paymentRoute"
import authRoute from "./routes/authRoute"


const app = express();

app.use(morgan("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(passport.initialize());
passport.use(passportMiddleware);

app.use("/api", userRoute);
app.use("/api", eventRoute);
app.use("/api", bookingRoute);
app.use("/api", paymentRoute);
app.use("/api", authRoute);

export default app;
