import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  PrimaryColumn
} from "typeorm"
import { Booking } from "./Booking"


@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string

  @Column()
  email: string

  @Column({ default: "user" })
  userName: string

  @Column()
  password: string

  @Column({ default: true })
  active: boolean

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @OneToMany(() => Booking, (booking: Booking) => booking.user, { cascade: true })
  bookings: Booking[]
};
