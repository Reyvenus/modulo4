import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from "typeorm"
import { Booking } from "./Booking"
import { PaymentMethods } from "../constans/constans"


@Entity()
export class Payment extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string

  @Column({
    type: "enum",
    enum: PaymentMethods,
    default: null
  })
  paymentMethod: PaymentMethods

  @Column()
  totalAmount: number

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @OneToMany(() => Booking, (booking: Booking) => booking.payment, {cascade:true})
  bookings: Booking[]
};
