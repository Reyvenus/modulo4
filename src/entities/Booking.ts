import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  ManyToMany,
} from "typeorm"
import { User } from "./User"
import { Event } from "./Event"
import { Statuses } from "../constans/constans";
import { Payment } from "./Payment";

@Entity()
export class Booking extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string

  @Column()
  total_price: number

  @Column({
    type: "enum",
    enum: Statuses,
    default: Statuses.PENDING
  })
  status: Statuses

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @ManyToOne(() => User, (user: User) => user.bookings)
  user: User

  @ManyToMany(() => Event, (event) => event.bookings)
  events: Event[]

  @ManyToOne(() => Payment, (payment: Payment) => payment.bookings)
  payment: Payment
};
