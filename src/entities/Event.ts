import { Booking } from "./Booking";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  JoinTable,
  ManyToMany,
} from "typeorm";


@Entity()
export class Event extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  place: string;

  @Column()
  date: string;

  @Column()
  gps: string;

  @Column()
  price: number;

  @Column({ default: 10 })
  top_limit: number;

  @Column({ default: 0 })
  lower_limit: number;

  @Column()
  eventType: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToMany(() => Booking, (booking) => booking.events, { cascade: true })
  @JoinTable()
  bookings: Booking[]

};
