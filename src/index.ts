import app from "./app"
import "reflect-metadata"
import { AppDataSource } from "./db"
import dotenv from "dotenv";
dotenv.config();

const PORT = 3000;

const main = async () => {
  try {
    await AppDataSource.initialize();
    app.listen(PORT, () => console.log(`Server listening in port ${PORT}`));
  } catch (error) {
    console.log("ERRRROOOORRRR", error);
    throw error;
  }
};

main();
