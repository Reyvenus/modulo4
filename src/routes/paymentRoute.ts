import { Router } from "express";
import { createPayment, getPayment } from "../controller/payment.controller";
import passport from "passport";


const router = Router();

router.post("/payment", passport.authenticate('jwt', { session: false }), createPayment);
router.get("/payment", passport.authenticate('jwt', { session: false }), getPayment);
router.get("/payment/:paymentId", passport.authenticate('jwt', { session: false }), getPayment);

export default router;
