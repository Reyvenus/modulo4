import { Router } from "express";
import { createEvent, getEvent, updateEvent, deleteEvent } from "../controller/event.controller";
import passport from "passport";


const router = Router();

router.get("/event", getEvent);
router.get("/event/:eventId", passport.authenticate('jwt', { session: false }), getEvent);
router.post("/event", passport.authenticate('jwt', { session: false }), createEvent);
router.put("/event/:eventId", passport.authenticate('jwt', { session: false }), updateEvent);
router.delete("/event/:eventId", passport.authenticate('jwt', { session: false }), deleteEvent);

export default router;
