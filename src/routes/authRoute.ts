import { Router } from "express"
import { signIn, refresh } from "../controller/auth.controller"


const router = Router();

router.post("/auth/signin", signIn);
router.post("/auth/token", refresh);

export default router;
