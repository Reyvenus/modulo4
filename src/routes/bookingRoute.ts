import { Router } from "express";
import { createBooking, getBooking, updateBooking, deleteBooking } from "../controller/booking.controller";
import passport from "passport";


const router = Router()

router.get("/booking", passport.authenticate('jwt', { session: false }), getBooking)
router.get("/booking/:bookingId", passport.authenticate('jwt', { session: false }), getBooking)
router.post("/booking/:userId", passport.authenticate('jwt', { session: false }), createBooking)
router.put("/booking/:bookingId", passport.authenticate('jwt', { session: false }), updateBooking)
router.delete("/booking/:bookingId", passport.authenticate('jwt', { session: false }), deleteBooking)

export default router
