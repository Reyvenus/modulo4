import { Router } from "express";
import passport from "passport";
import { getUsers, getUser, createUser, deleteUser, updateUser } from "../controller/user.controller";


const router = Router();

router.get("/users", passport.authenticate('jwt', { session: false }), getUsers)
router.get("/user/:userId", passport.authenticate('jwt', { session: false }), getUser);
router.post("/user", createUser);
router.put("/user/:userId", passport.authenticate('jwt', { session: false }), updateUser);
router.delete("/user/:userId", passport.authenticate('jwt', { session: false }), deleteUser);

export default router;
