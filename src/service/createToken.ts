import jwt from "jsonwebtoken";
import { User } from "../entities/User";
import dotenv from "dotenv";
dotenv.config();

export const jwtSecret = 'somesecrettoken';
export const jwtRefreshTokenSecret = 'somesecrettokenrefresh';
export let refreshTokens: (string | undefined)[] = [];


export const createToken = (user: User) => {
  const token = jwt.sign({ id: user.id, email: user.email }, jwtSecret, { expiresIn: '3600s' });
  const refreshToken = jwt.sign({ email: user.email }, jwtRefreshTokenSecret, { expiresIn: '90d' });

  refreshTokens.push(refreshToken);
  return {
    token,
    refreshToken
  }
};
