export enum Statuses {
  PENDING = "pending",
  PROCESSED = "processed",
};

export enum PaymentMethods {
  DEBIT_CARD = "debit_card",
  CREDIT_CRAD = "credit_card",
  CASH = "cash",
  TRANSFER = "transfer"
}

export const paymentTypes = ["debit_card", "credit_card", "cash", "transfer"]
