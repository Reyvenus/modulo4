import { DataSource } from "typeorm";
import { User } from "./entities/User";
import { Event } from "./entities/Event";
import { Booking } from "./entities/Booking";
import { Payment } from "./entities/Payment";


export const AppDataSource = new DataSource({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "mysql",
  database: "sales and reservation system - db",
  logging: true, 
  synchronize: true,
  entities: [User, Event, Booking, Payment],
});